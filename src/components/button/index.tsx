import * as React from 'react';
import styles from './index.scss';
import { combineClasses } from '../../utils';

/**
 * An example of extending the underlying the props and exposing additional information
 * to suit your custom extended component.
 */
export interface IComponentProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  variant: 'primary' | 'secondary' | 'warning' | 'error' | 'success' | 'menu';
}

/**
 * An example of creating a customised element.
 * @param props     The props to pass through to the component.
 */
export const Button: React.FunctionComponent<IComponentProps> = (props) => {
  const {
    variant,
    children,
    className,
    ...otherProps
  } = props;

  const classes = combineClasses(styles['os-button'], className || '');
  return (
    <button {...otherProps} className={classes} data-variant={variant}>
      { children }
    </button>
  );
};

export default Button;

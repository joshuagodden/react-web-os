import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { RouteOS } from './routes/os';
import { RouteLogin } from './routes/login';
import { GlobalProvider } from './global';
import styles from './index.scss';

ReactDOM.render(
  <GlobalProvider>
    <BrowserRouter>
      <main id={styles['web-os']}>
        <Routes>
          <Route path="/" element={<RouteLogin />} />
          <Route path="/os" element={<RouteOS />} />
        </Routes>
      </main>
    </BrowserRouter>
  </GlobalProvider>,
  document.querySelector('#root'),
);
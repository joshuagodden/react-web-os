import { GlobalActionType, IUser } from '@WebOS/models';

export type ActionLogin = {
  type: GlobalActionType.LOGIN_USER,
  user: IUser,
};

export type ActionLogout = {
  type: GlobalActionType.LOGOUT_USER,
}

export type ActionTaskOpen = {
  type: GlobalActionType.TASK_OPEN,
  applicationId: string,
}

export type ActionTaskClose = {
  type: GlobalActionType.TASK_CLOSE,
  taskId: string,
}

export type ActionTaskMinimize = {
  type: GlobalActionType.TASK_MINIMIZE,
  taskId: string,
}

export type ActionTaskMaximize = {
  type: GlobalActionType.TASK_MAXIMIZE,
  taskId: string,
}

export type GlobalAction = ActionLogin | ActionLogout | ActionTaskOpen | ActionTaskClose | ActionTaskMinimize | ActionTaskMaximize;
import * as React from 'react';
import { IGlobalState } from '@WebOS/models';
import { GlobalAction } from './actions';
import { globalReducer } from './reducer';

/**
 * Default state.
 */
export const DEFAULT_STATE: IGlobalState = {
  tasks: [],
  applications: [
    {
      id: 'ec52306f-1fc6-4a97-ad68-909b9d5f24d2',
      name: 'Text Editor',
      defaultHeight: 200,
      defaultWidth: 200,
    },
  ],
};

/**
 * Context for the global state.
 */
const GlobalStateContext = React.createContext<IGlobalState | null>(null);

/**
 * Context for the dispatcher to invoke on global state.
 */
const GlobalDispatchContext = React.createContext<React.Dispatch<GlobalAction> | null>(null);

/**
 * The GlobalProvider has to be applied at the highest level of the React tree.
 * Provides a global context to the entire application so global can be used at whatever point in a component.
 * @param props     The props to pass down to the Provider
 */
export const GlobalProvider: React.FunctionComponent<{ children: unknown }> = (props) => {
  const { children } = props;
  const [state, dispatch] = React.useReducer(globalReducer, DEFAULT_STATE);
  return (
    <GlobalStateContext.Provider value={state}>
      <GlobalDispatchContext.Provider value={dispatch}>
        { children }
      </GlobalDispatchContext.Provider>
    </GlobalStateContext.Provider>
  );
};

/**
 * Use the global state.
 * @returns   A reference to the global state.
 */
export const useState = (): IGlobalState => {
  const context = React.useContext(GlobalStateContext);
  
  if (context === null) {
    throw new Error('useGlobalState::No Provider was found in the tree.');
  }

  return context;
};

/**
 * Use the global dispatcher to invoke actions against the global state.
 * @returns   A reference to the global dispatcher.
 */
export const useDispatch = (): React.Dispatch<GlobalAction> => {
  const context = React.useContext(GlobalDispatchContext);

  if (context === null) {
    throw new Error('useGlobalDispatch::No Provider was found in the tree.');
  }

  return context;
}

/**
 * Use both the global state and dispatcher/
 * @returns   A reference to both the global state and dispatcher.
 */
export const useGlobal = (): [IGlobalState, React.Dispatch<GlobalAction>] => {
  const state = useState();
  const dispatch = useDispatch();
  return [state, dispatch];
};

export default {
  useState,
  useDispatch,
  useGlobal,
  GlobalProvider,
};

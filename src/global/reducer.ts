import { GlobalActionType, IGlobalState } from '@WebOS/models';
import { GlobalAction } from './actions';
import { generateID, generateNumber } from '../utils';

/**
 * The reducer for all global actions.
 * This is a prime example of how a reducer would work when calling actions against React context flux/state.
 * 
 * Once you have nultiple applications all requiring the global state, you would seperate these apps or logical brains
 * into their own reducers which can then be combined into one reducer pattern.
 */
export const globalReducer = (state: IGlobalState, action: GlobalAction): IGlobalState => {
  switch (action.type) {
    case GlobalActionType.LOGIN_USER:
      return {
        ...state,
        user: {
          ...action.user,
        },
      };
    case GlobalActionType.LOGOUT_USER:
      return {
        ...state,
        user: undefined,
      };
    case GlobalActionType.TASK_OPEN:
      const taskId = generateID();
      return {
        ...state,
        focusedTaskId: taskId,
        tasks: [
          ...state.tasks,
          {
            id: taskId,
            applicationId: action.applicationId,
            isMaximized: false,
            isMinimized: false,
          },
        ],
      };
    case GlobalActionType.TASK_CLOSE:
      return {
        ...state,
        tasks: state.tasks.filter((task) => task.id !== action.taskId),
      };
    case GlobalActionType.TASK_MINIMIZE:
      return {
        ...state,
        tasks: state.tasks.map((task) => {
          if (task.id === action.taskId) {
            return {
              ...task,
              isMinimized: !task.isMinimized,
            };
          }

          return task;
        }),
      }
    case GlobalActionType.TASK_MAXIMIZE:
      return {
        ...state,
        tasks: state.tasks.map((task) => {
          if (task.id === action.taskId) {
            return {
              ...task,
              isMaximized: !task.isMaximized,
            };
          }

          return task;
        }),
      }
    default:
      return state;
  }
};

export default globalReducer;

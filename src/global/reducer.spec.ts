import { GlobalActionType } from '@WebOS/models';
import { DEFAULT_STATE } from './index';
import { globalReducer } from './reducer';

/**
 * An example of testing reducers, checking that these reducers
 * are purely functional.
 */
describe('globalReducer', () => {
  it('can log the user in', () => {
    const initalState = {
      ...DEFAULT_STATE,
    };

    const state = globalReducer(initalState, {
      type: GlobalActionType.LOGIN_USER,
      user: {
        firstName: 'Joe',
        lastName: 'Bloggs',
      },
    });

    expect(state.user).toEqual({
      firstName: 'Joe',
      lastName: 'Bloggs',
    });
  });

  it('can log the user out', () => {
    const initalState = {
      ...DEFAULT_STATE,
      user: {
        firstName: 'Joe',
        lastName: 'Bloggs',
      }
    }

    const state = globalReducer(initalState, {
      type: GlobalActionType.LOGOUT_USER,
    });

    expect(state.user).toBeNull();
  });

  /**
   * An example of using an expecting object to
   * find key properties in an object.
   */
  it('can create a new task', () => {
    const state = globalReducer(DEFAULT_STATE, {
      type: GlobalActionType.TASK_OPEN,
      applicationId: 'text-editor',
    });
    
    expect(state.tasks).toEqual([
      expect.objectContaining({
        applicationId: 'text-editor',
        isMaximized: false,
        isMinimized: false,
      }),
    ]);
  });
});
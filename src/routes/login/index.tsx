import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import styles from './index.scss';
import { Button } from '../../components/button';
import { useGlobal } from '../../global';
import { GlobalActionType } from '@WebOS/models';

export const RouteLogin: React.FunctionComponent = () => {
  const navigate = useNavigate();
  const [state, dispatch] = useGlobal();
  const [loginDetails, setLoginDetails] = React.useState<string>();
  const [isValidating, setIsValidating] = React.useState<boolean>(false);
  const [error, setError] = React.useState<string>();
  const { user } = state;

  // effect - listen for user changes
  // - important as if you end up back at login, but already logged in, redirect us back to the app
  React.useEffect(() => {
    if (typeof (user) !== 'undefined') {
      navigate('/os');
      console.log('hit 2');
    }
  }, [user]);

  // effect - listen out for login detail and pending changes so we know to send a server request
  React.useEffect(() => {
    if (loginDetails && isValidating) {
      try {
        
        // just pretend we have a backend and security was all figured out at this point
        if (loginDetails !== 'admin:admin') {
          throw new Error('User is not registered.');
        }

        dispatch({
          type: GlobalActionType.LOGIN_USER,
          user: {
            firstName: 'Super',
            lastName: 'Admin',
          },
        });
      } catch (e: any) {
        setError(e.message);
      } finally {
        console.log('hit?');
        setLoginDetails(undefined);
        setIsValidating(false);
      }
    }
  }, [loginDetails, isValidating]);

  // event - handle the submit and set some details
  const handleSubmit = (ev: React.FormEvent<HTMLFormElement>): void => {
    ev.preventDefault();

    // 01. grab all inputs, convert to standard Array as NodeLists are not great to use iterative array functions on
    const elements = Array
      .from(ev.currentTarget.elements)
      .filter(i => i.nodeName === 'INPUT') as Array<HTMLInputElement>;

    // 02. convert it to a standard record
    const { username, password } = elements
      .reduce((current, next) => {
        const key = next.getAttribute('data-key') as string;
        return {
          ...current,
          [key]: next.value,
        };
      }, {} as Record<string, unknown>);

    // 03. store the data in local state so it kicks off an effect
    setLoginDetails(`${username}:${password}`);
    setIsValidating(true);
  };

  return (
    <section id={styles['route-login']}>
      <article id={styles['route-login--form-container']}>
        <header>
          <h2>Welcome to WebOS!</h2>
          <h3>Please enter your login details to access your operating system.</h3>
        </header>
        {
          error
            ? <p className={styles['error']}>{error}</p>
            : <br />
        }
        <form onSubmit={handleSubmit}>
          <label htmlFor="login-username">Username</label>
          <input id="login-username" type="text" data-key="username" />
          <label htmlFor="login-password">Password</label>
          <input id="login-password" type="password" data-key="password"/>
          <Button type="submit" variant="primary">Login</Button>
        </form>
      </article>
    </section>
  );
};

export default RouteLogin;

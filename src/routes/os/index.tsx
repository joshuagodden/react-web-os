import * as React from 'react';
import { GlobalActionType, IApplication } from '@WebOS/models';
import { useGlobal } from '../../global';
import { Button } from '../../components/button';
import styles from './index.scss';

/**
 * An example of a pure function retrieving an application from a given set of applications.
 * @param id              The application ID used to find an application,
 * @param applications    The applications to filter through.
 * @throws                An Error if no application is found.
 * @returns               The application found.
 */
 function getApplicationById(id: string, applications: Array<IApplication>): IApplication {
  const application = applications.find((application) => application.id === id);
  if (application === null || application === undefined) {
    throw new Error(`Cannot find an application with idenifiter ${id}.`);
  }

  return application;
}

/**
 * This is a really basic web-os interface that was made to demonstrate various
 * React techniques that could be used.
 * 
 * We can see interactions with both a global and internal state.
 * Along with iterative component mapping that can be dynamically injected with content.
 * @returns 
 */
export const RouteOS: React.FunctionComponent = () => {
  const [state, dispatch] = useGlobal();
  const [isStartOpen, setIsStartOpen] = React.useState<boolean>(false);
  const { applications, tasks } = state;

  // event - handle start button clicked
  const handleStartClick = () => {
    setIsStartOpen(!isStartOpen);
  };

  // event - handle menu item click
  const handleMenuItemClick = (ev: React.MouseEvent<HTMLButtonElement>): void => {
    const id = ev.currentTarget.parentElement?.getAttribute('data-id') as string;
    setIsStartOpen(false);

    dispatch({
      type: GlobalActionType.TASK_OPEN,
      applicationId: id,
    });
  };

  // event - handle a task closing
  const handleTaskClose = (ev: React.MouseEvent<HTMLButtonElement>): void => {
    const id = ev.currentTarget.closest('article')?.getAttribute('data-id') as string;
    dispatch({
      type: GlobalActionType.TASK_CLOSE,
      taskId: id,
    });
  };

  return (
    <section id={styles['route-os']}>
      <header id={styles['route-os--taskbar']}>
        <Button type="button" variant="menu" onClick={handleStartClick}>Start</Button>
        {
          tasks.map((task, index) => {
            if (index > 0) { // for demo purposes, only show one
              return null;
            }

            const application = getApplicationById(task.applicationId, applications);
            return (
              <Button type="button" variant="menu">
                { application?.name }
              </Button>
            );
          })
        }
        {
          isStartOpen && (
            <nav id={styles['route-os--start-menu']}>
              <ul>
                {
                  applications.map((application) => (
                    <li data-id={application.id}>
                      <Button type="button" variant="menu" onClick={handleMenuItemClick}>
                        { application.name }
                      </Button>
                    </li>
                  ))
                }
              </ul>
            </nav>
          )
        }
      </header>
      <section id={styles['route-os--workspace']}>
        {
          tasks.map((task, index) => {
            if (index > 0) { // for demo purposes, only show one
              return null;
            }

            const application = getApplicationById(task.applicationId, applications);
            return (
              <article className={styles['workspace-task']} data-id={task.id}>
                <header>
                  <p>{ application.name }</p>
                </header>
                <section>
                  Content goes here
                  <Button type="button" variant="error" onClick={handleTaskClose}>
                    Destroy
                  </Button>
                </section>
              </article>
            )
          })
        }
      </section>
    </section>
  );
};

export default RouteOS;

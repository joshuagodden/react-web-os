import {
  generateNumber,
  generateID,
  clamp,
} from './index';

/**
 * An example of unit tests, testing just pure functionality from utilities.
 */
describe('utils', () => {
  it('can generate a random number', () => {
    const num1 = generateNumber(100);
    const num2 = generateNumber(100);
    expect(num1).not.toEqual(num2);
  });

  it('can generate a unique ID', () => {
    const id1 = generateID();
    const id2 = generateID();
    expect(id1).not.toEqual(id2);
  });

  describe('clamp', () => {
    it('can clamp a minimum number', () => {
      const number = -2;
      const clamped = clamp(number, 0, 10);
      expect(clamped).toEqual(0);
    });

    it('can clamp a maximum number', () => {
      const number = 20;
      const clamped = clamp(number, 0, 10);
      expect(clamped).toEqual(10);
    });

    it('can return a non clamped number', () => {
      const number = 5;
      const clamped = clamp(number, 0, 10);
      expect(clamped).toEqual(5);
    });
  });
});
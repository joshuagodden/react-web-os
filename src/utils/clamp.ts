/**
 * Clamps a given number.
 * @param num   The number to clamp.
 * @param min   The minimum number to clamp the given number.
 * @param max   The maximum number to clamp the given number.
 * @returns     A clamped (if possible) number.
 */
export function clamp(num: number, min: number, max: number): number {
  if (num < min) {
    return min;
  }

  if (num > max) {
    return max;
  }

  return num;
}

export default clamp;

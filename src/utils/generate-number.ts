/**
 * Generate a random rounded number
 * @param size    Size of the random number.
 * @returns       Returns a randomly rounded number.
 */
export function generateNumber(size: number): number {
  return Math.floor(Math.random() * size);
}

export default generateNumber;

/**
 * Combines classes and safely joins and trims them.
 * @param classes     The classes to combine.
 * @returns           A combined string of classes.
 */
export function combineClasses(...classes: string[]): string {
  return classes.join(' ').trim();
}

export default combineClasses;

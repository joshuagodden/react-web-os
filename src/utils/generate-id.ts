import { v4 as uuidv4 } from 'uuid';

/**
 * A very questionable ID generator. Just for demonstration purposes.
 */
export function generateID(): string {
  return uuidv4();
}

export default generateID;

const path = require('path');
const base = require('./webpack.config');

const config = {
  ...base,
  devServer: {
    static: {
      directory: path.join(__dirname, 'public'),
    },
    historyApiFallback: true,
    compress: true,
    port: 9000,
  },
};

module.exports = config;

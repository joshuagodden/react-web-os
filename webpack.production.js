const base = require('./webpack.config');

const config = {
  ...base,
  mode: 'production',
};

module.exports = config;

export interface ITask {
  id: string;
  applicationId: string;
  isMinimized: boolean;
  isMaximized: boolean;
}
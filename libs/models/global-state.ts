import { IApplication } from './application';
import { ITask } from './task';
import { IUser } from './user';

export interface IGlobalState {
  user?: IUser;
  tasks: Array<ITask>;
  applications: Array<IApplication>;
  focusedTaskId?: string;
}

export default IGlobalState;

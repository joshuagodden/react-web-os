export interface IApplication {
  id: string;
  name: string;
  defaultWidth?: number;
  defaultHeight?: number;
}
export enum GlobalActionType {
  LOGIN_USER = 'login:user',
  LOGOUT_USER = 'logout:user',
  TASK_FOCUS = 'task:focus',
  TASK_OPEN = 'task:open',
  TASK_CLOSE = 'task:close',
  TASK_MINIMIZE = 'task:minimze',
  TASK_MAXIMIZE = 'task:maximize',
}